const express = require('express');
const app = express();
const port = 8000;


app.listen(port,()=>{
    console.log("Projeto executando na porta: " + port);
});


app.get('/employee',(req, res)=>{
    console.log("Acessando o recurso employee");
    res.send("{message:/get employee}");
});

app.get('/search',(req, res)=>{
    let dados = req.query;
    res.send(dados.nome + " " + dados.sobrenome);
});

app.get('/search/client/:code',(req, res)=>{
    let id = req.params.codigo;
    res.send("Dados do cliente " + id);
});

const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
app.post('/funcionario/gravar',(req, res)=>{
    let valores = req.body;
    console.log("body: " + req);
    console.log("req.body: " + req.body);
    console.log("valores: " + valores);
    console.log("Sobrenome: " + valores.sobrenome);
    console.log("Idade: "+ valores.idade);
    res.send("suCelsio");
});

